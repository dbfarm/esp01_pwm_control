#include "osapi.h"
#include "at_custom.h"
#include "user_interface.h"

#include "ets_sys.h"
#include "os_type.h"
#include "mem.h"
#include "espconn.h"

#include "cmd_esp8266.h"

#include "pwm.h"


/*****************************************************************************
 * 因为这几个函数无法直接放在CmdTbl中调用，所以重新封装一次
 */
void ICACHE_FLASH_ATTR *_malloc(uint32_t size)
{
	return (void *)os_malloc(size);
}
void ICACHE_FLASH_ATTR _free(void *p)
{
	os_free(p);
}
void ICACHE_FLASH_ATTR *_memset(void *s, int c, uint32_t n)
{
	os_memset(s, c, n);
	return (void *)s;
}
void ICACHE_FLASH_ATTR delay_us(uint32_t us)
{
	os_delay_us(us);
}
void ICACHE_FLASH_ATTR get_ip_mac(void)
{
	char hwaddr[6];
	struct ip_info ipconfig;

	wifi_get_ip_info(SOFTAP_IF, &ipconfig);
	wifi_get_macaddr(SOFTAP_IF, hwaddr);
	os_printf("soft-AP:" MACSTR " " IPSTR, MAC2STR(hwaddr), IP2STR(&ipconfig.ip));

	wifi_get_ip_info(STATION_IF, &ipconfig);
	wifi_get_macaddr(STATION_IF, hwaddr);
	os_printf("\nstation:" MACSTR " " IPSTR "\n", MAC2STR(hwaddr), IP2STR(&ipconfig.ip));
}






/******************************************************************************
 *  pwm测试
 */

#define PWM_0_OUT_IO_MUX	PERIPHS_IO_MUX_GPIO0_U
#define PWM_0_OUT_IO_NUM	0
#define PWM_0_OUT_IO_FUNC	FUNC_GPIO0

#define PWM_1_OUT_IO_MUX	PERIPHS_IO_MUX_GPIO2_U
#define PWM_1_OUT_IO_NUM	2
#define PWM_1_OUT_IO_FUNC	FUNC_GPIO2

#define PWM_2_OUT_IO_MUX	PERIPHS_IO_MUX_MTDI_U
#define PWM_2_OUT_IO_NUM	12
#define PWM_2_OUT_IO_FUNC	FUNC_GPIO12

#define PWM_3_OUT_IO_MUX	PERIPHS_IO_MUX_MTCK_U
#define PWM_3_OUT_IO_NUM	13
#define PWM_3_OUT_IO_FUNC	FUNC_GPIO13

#define PWM_4_OUT_IO_MUX	PERIPHS_IO_MUX_MTMS_U
#define PWM_4_OUT_IO_NUM	14
#define PWM_4_OUT_IO_FUNC	FUNC_GPIO14

#define PWM_5_OUT_IO_MUX	PERIPHS_IO_MUX_MTDO_U
#define PWM_5_OUT_IO_NUM	15
#define PWM_5_OUT_IO_FUNC	FUNC_GPIO15

#define PWM_CHANNEL		6

uint32_t pwm_value_current[PWM_CHANNEL];//pwm当前值
uint32_t pwm_value_target[PWM_CHANNEL];//pwm渐变目标值
uint8_t pwm_gradually_dir[PWM_CHANNEL];	//pwm渐变的方向, [7]:1:渐变未完成, 0:渐变完成

os_timer_t pwm_test_timer;

static void ICACHE_FLASH_ATTR
pwm_cb(void *timer_arg)
{
	uint8_t i;
	uint8_t flag = 0;
	int32_t step = 22222/100; // 1% 的步进值, 用有符号数
//	uint32_t step = 22222/100; // 1% 的步进值
	
	for (i = 0; i < PWM_CHANNEL; i++)
	{
		if (pwm_gradually_dir[i] & 0x80)
		{
			if (pwm_gradually_dir[i] & 0x01)
			{
				pwm_value_current[i] += step;
			}
			else
			{
				//注意不要减成负数了
				if (pwm_value_current[i] > step)
				{
					pwm_value_current[i] -= step;
				}
				else
				{
					pwm_value_current[i] = 0;
					pwm_value_target[i] = 0;
				}
			}
			pwm_set_duty(pwm_value_current[i], i);

			//判断渐变是否完成, 这里不能用等号来判断，因为运算公式的原因, 极个别情况下会出现不等情况
			int32_t r = pwm_value_current[i] - pwm_value_target[i];
			if ((-step < r) && (r < step))
			//为什么这个条件不会成立� 下面这句却是成立的? 无符号数的原因? step为无符号数时, -step等于多少?
//			if ((-222 < r) && (r < 222))	//这句话会成立
			{
				pwm_gradually_dir[i] = 0;
			}
			else
			{
				flag = 1;
			}

			os_printf("%d: cur=%d tar:%d r:%d\n", i, pwm_value_current[i], pwm_value_target[i], r);
		}
	}
	pwm_start();

	//任意通道还没有渐变完成，则继续触发pwm事件
	if (flag)
	{
		os_timer_disarm(&pwm_test_timer);
		os_timer_setfn(&pwm_test_timer, (os_timer_func_t *)pwm_cb, 0);
		os_timer_arm(&pwm_test_timer, 10, 0);
	}
}

/*
 * 每一次配置pwm参数后，都要调用pwm_start()重新启动
 * @period  1000us(1khz)~ 10000us(100hz)
 * @duty    22222       ~ 222222 ,  公式 duty = period*1000/45
 *
 * note     自带的pwm模块会有一个很小的脉冲，并且占空比无法调到100%
 */
void ICACHE_FLASH_ATTR ledpwm_test_init(void)
{
	uint32 io_info[PWM_CHANNEL][3] =
	{
		{PWM_0_OUT_IO_MUX, PWM_0_OUT_IO_FUNC, PWM_0_OUT_IO_NUM},
		{PWM_1_OUT_IO_MUX, PWM_1_OUT_IO_FUNC, PWM_1_OUT_IO_NUM},
		{PWM_2_OUT_IO_MUX, PWM_2_OUT_IO_FUNC, PWM_2_OUT_IO_NUM},
		{PWM_3_OUT_IO_MUX, PWM_3_OUT_IO_FUNC, PWM_3_OUT_IO_NUM},
		{PWM_4_OUT_IO_MUX, PWM_4_OUT_IO_FUNC, PWM_4_OUT_IO_NUM},
		{PWM_5_OUT_IO_MUX, PWM_5_OUT_IO_FUNC, PWM_5_OUT_IO_NUM},
	};
	uint32 duty[PWM_CHANNEL] = {0, 0, 0, 0, 0, 0}; //初始pwm占空比
	//pwm频率初始为1KHz
	pwm_init(1000,  duty , PWM_CHANNEL, io_info);

	uint8_t i;
	for (i = 0; i < PWM_CHANNEL; i++)
	{
		pwm_value_current[i] = 0;
		pwm_value_target[i] = 0;
		pwm_gradually_dir[i] = 0;
	}
}

/*
 * pwm占空比设置(渐变)
 * @duty 0-100
 * @channel 0-5
 */
void ICACHE_FLASH_ATTR
ledpwm_set_duty_gradually(uint32 duty, uint8 channel)
{
	if ((duty > 100) || (channel > PWM_CHANNEL-1))
	{
		return ;
	}

	//设置目标pwm, 1KHz下最大的duty值是22222
	pwm_value_target[channel] = (duty * 22222) / 100;

	//判断渐变方向
	if (pwm_value_target[channel] > pwm_value_current[channel])
	{
		pwm_gradually_dir[channel] = 0x81;
	}
	else if (pwm_value_target[channel] < pwm_value_current[channel])
	{
		pwm_gradually_dir[channel] = 0x80;
	}
	else
	{
		pwm_gradually_dir[channel] = 0;
	}

	os_timer_disarm(&pwm_test_timer);
	os_timer_setfn(&pwm_test_timer, (os_timer_func_t *)pwm_cb, 0);
	os_timer_arm(&pwm_test_timer, 0, 0);//0ms表示马上超时执行
}

/*
 * pwm占空比设置, 直接设置
 * @duty  0-100
 * @channel 0-5
 */
void ICACHE_FLASH_ATTR
ledpwm_set_duty(uint32 duty, uint8 channel)
{
	if ((duty > 100) || (channel > PWM_CHANNEL-1))
	{
		return ;
	}

	duty = (duty * 22222) / 100;
	pwm_value_current[channel] = duty;
	pwm_value_target[channel] = duty;

	pwm_set_duty(duty, channel);
	pwm_start();
}








#ifndef _CMD_ESP8266_H_
#define _CMD_ESP8266_H_


void *_malloc(uint32_t size);
void _free(void *p);
void *_memset(void *s, int c, uint32_t n);
void delay_us(uint32_t us);
void get_ip_mac(void);


void ledpwm_test_init(void);
void ledpwm_set_duty(uint32 duty, uint8 channel);
void ledpwm_set_duty_gradually(uint32 duty, uint8 channel);

#endif


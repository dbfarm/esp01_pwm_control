#ifndef _FCMD_CFG_H_
#define _FCMD_CFG_H_
/*******************************************************************************
* 用户函数命令头文件包含，函数声明
*/

#include "cmd_mem.h"
#include "osapi.h"

#include "user_interface.h"
#include "mem.h"
#include "cmd_esp8266.h"
#include "flash_api.h"
#include "pwm.h"


/*******************************************************************************
 * 自定义函数命令表
 */
CmdTbl_t CmdTbl[] =
{
	//系统命令, SYSTEM_CMD_NUM和系统命令个数保持一致
	"ls",  0,
	"addr", 0,
	"help", 0,


	//用户命令
	"void md(int addr, int elem_cnt, int elem_size)", (void(*)(void))md,
	"int cmp(void *addr1, void *addr2, int elem_cnt, int elem_size)", (void(*)(void))cmp,


	//system api
	"uint32 system_get_free_heap_size(void)", (void(*)(void))system_get_free_heap_size,
	"uint32 system_get_chip_id(void)", (void(*)(void))system_get_chip_id,
	"void system_restart(void)", (void(*)(void))system_restart,
	"void system_restore(void)",  (void(*)(void))system_restore,

	"void get_ip_mac(void)", (void(*)(void))get_ip_mac,

	"void *_malloc(uint32_t size)", (void(*)(void))_malloc,
	"void _free(void *p)", (void(*)(void))_free,
	"void *_memset(void *s, int c, uint32_t n)",  (void(*)(void))_memset,
	"void delay_us(uint32_t us)",	(void(*)(void))delay_us,

	// flash
	"uint32 spi_flash_get_id(void)",   (void(*)(void))spi_flash_get_id,
	"int sfmd(u32 addr, int elem_cnt, int elem_size)",	(void(*)(void))sfmd,
	"int sfmw(u32 writeaddr, u8 *pbuf, u32 num)",		(void(*)(void))sfmw,


	"void ledpwm_test_init(void)",	(void(*)(void))ledpwm_test_init,
	"void ledpwm_set_duty(uint32 duty, uint8 channel)",	(void(*)(void))ledpwm_set_duty,
	"void ledpwm_set_duty_gradually(uint32 duty, uint8 channel)", (void(*)(void))ledpwm_set_duty_gradually,
	
	"void pwm_start(void)",		(void(*)(void))pwm_start,
	"void pwm_set_duty(uint32 duty, uint8 channel)", (void(*)(void))pwm_set_duty,
	"uint32 pwm_get_duty(uint8 channel)", (void(*)(void))pwm_get_duty,
	"void pwm_set_period(uint32 period)",	(void(*)(void))pwm_set_period,
	"uint32 pwm_get_period(void)",		(void(*)(void))pwm_get_period,

	"uint16 system_adc_read(void)",	(void(*)(void))system_adc_read,
	"uint16 system_get_vdd33(void)", (void(*)(void))system_get_vdd33,

};
uint8_t CmdTblSize = sizeof(CmdTbl) / sizeof(CmdTbl[0]);

#endif



/******************************************************************************
 * Copyright 2013-2014 Espressif Systems (Wuxi)
 *
 * FileName: user_main.c
 *
 * Description: entry file of user application
 *
 * Modification history:
 *     2015/1/23, v1.0 create this file.
*******************************************************************************/

#include "osapi.h"
#include "at_custom.h"
#include "user_interface.h"

#include "ets_sys.h"
#include "os_type.h"
#include "mem.h"
#include "espconn.h"
#include "pwm.h"

#include "uart.h"

ETSTimer tm;//创建定时器
ETSTimer test_timer;
ETSTimer connect_timer;

struct espconn user_udp_espconn;

uint32 pwm_duty[1]= {0};
uint32 io_info[1][3]={
{PERIPHS_IO_MUX_MTDI_U,FUNC_GPIO2,2}
};

void ICACHE_FLASH_ATTR user_udp_send(void){//UDP发送函数
    char yladdr[6];
    char DeviceBuffer[40]={0};//将获取的MAC地址格式化输出到一个buffer里面
		wifi_get_macaddr(STATION_IF,yladdr);//查询MAC地址
		os_sprintf(DeviceBuffer,"设备地址为"MACSTR"!!!\r\n",MAC2STR(yladdr));//格式化MAC地址
		espconn_sent(&user_udp_espconn,DeviceBuffer,os_strlen(DeviceBuffer));
}

void ICACHE_FLASH_ATTR user_udp_sent_cb(void *arg){//发送回调函数
    os_printf("\r\nUDP发送成功！\r\n");
    os_timer_disarm(&test_timer);//定个时发送
    os_timer_setfn(&test_timer,user_udp_send,NULL);
    os_timer_arm(&test_timer,10000,NULL);//定10秒钟发送一次
}

int StringToInt(char *dat,unsigned short size)
{
	int a=0;
	unsigned short i;
	unsigned short len=size;
	if(len>5)len=5;
	if(len==0)return 0;
	if(dat[0]!='-')
	{
		for(i=0;i<len;i++)
		{
			if((dat[i]>=0x30)&&(dat[i]<=0x39))//0--9
			{
				unsigned int x=1;
				unsigned char y;
				for(y=1;y<(len-i);y++)
				{
					x = x*10;
				}
				a += x*(dat[i]-0x30);
			}
		}
		return a;
	}
}

void ICACHE_FLASH_ATTR user_udp_recv_cb(void *arg,
        char *pdata,
        unsigned short len){//接收回调函数
    os_printf("UDP已经接收数据：%s",pdata);//UDP接收到的数据打印出来
    if(len>10)
    {
		if((pdata[0]=='{')&&(pdata[2]=='P')&&(pdata[len-1]=='}'))
		{
			pwm_duty[0] = StringToInt(&pdata[9],len-10)*100;
			if(pwm_duty[0]<=100)
			{
				pwm_duty[0] = 0;
			}
			//pwm_init(1000,pwm_duty,1,io_info);

			pwm_set_duty(pwm_duty[0],0);
			pwm_start();

		}
    }
}

void ICACHE_FLASH_ATTR Wifi_conned(void *arg){
    static uint8 count=0;
    uint8 status;
    os_timer_disarm(&connect_timer);
    count++;
    status=wifi_station_get_connect_status();
    if(status==STATION_GOT_IP){
        os_printf("Wifi connect success!");//连接WiFi成功

        wifi_set_broadcast_if(STATIONAP_MODE);//设置UDP广播的发送接口station+soft-AP模式发送
        user_udp_espconn.type=ESPCONN_UDP;
        user_udp_espconn.proto.udp=(esp_udp*)os_zalloc(sizeof(esp_udp));
        user_udp_espconn.proto.udp->local_port=2525;
        user_udp_espconn.proto.udp->remote_port=1112;

        const char udp_remote_ip[4]={255,255,255,255};//用于存放远程IP地址
        os_memcpy(&user_udp_espconn.proto.udp->remote_ip,udp_remote_ip,4);

        espconn_regist_recvcb(&user_udp_espconn,user_udp_recv_cb);//接收回调函数
        espconn_regist_sentcb(&user_udp_espconn,user_udp_sent_cb);//发送回调函数
        espconn_create(&user_udp_espconn);//创建UDP连接
        user_udp_send(); //发送出去
        return;
    }else{
        if(count>=7){
        os_printf("Wifi connect fail！");
        return;
        }
    }
    os_timer_arm(&connect_timer,10000,NULL);
}

void ICACHE_FLASH_ATTR scan_done(void *arg,STATUS status){

     uint8 ssid[33];
      char temp[128];
      struct station_config stationConf;
      if (status == OK)
       {
         struct bss_info *bss_link = (struct bss_info *)arg;
         bss_link = bss_link->next.stqe_next;//ignore first

         while (bss_link != NULL)
         {
           os_memset(ssid, 0, 33);
           if (os_strlen(bss_link->ssid) <= 32)
           {
             os_memcpy(ssid, bss_link->ssid, os_strlen(bss_link->ssid));
           }
           else
           {
             os_memcpy(ssid, bss_link->ssid, 32);
           }
           os_sprintf(temp,"+CWLAP:(%d,\"%s\",%d,\""MACSTR"\",%d)\r\n",
                      bss_link->authmode, ssid, bss_link->rssi,
                      MAC2STR(bss_link->bssid),bss_link->channel);
           os_printf("%s",temp);
           bss_link = bss_link->next.stqe_next;
         }
        os_memcpy(&stationConf.ssid, "test", 32);
        os_memcpy(&stationConf.password, "wubing123", 64);
        wifi_station_set_config_current(&stationConf);
        wifi_station_connect();
        os_timer_setfn(&connect_timer,Wifi_conned,NULL);
        os_timer_arm(&connect_timer,2000,NULL);
       }
       else
       {
     //     os_sprintf(temp,"err, scan status %d\r\n", status);
     //     uart0_sendStr(temp);
        os_printf("%s","Error");
       }
}

void to_scan(void)  { wifi_station_scan(NULL,scan_done); }

void ICACHE_FLASH_ATTR
GetMyDuty(void *x)
{
	pwm_duty[0]+=100;
	if(pwm_duty[0]>=1000)
	{
		pwm_duty[0] = 0;
	}
	pwm_set_duty(pwm_duty[0],0);
	pwm_start();
	/////////////////////////
	//////////////////////////
}


void GPIO_Init()
{
//	pin_func_select(PERIPHS_IO_MUX_GPIO2_U,FUNC_GPIO2);
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO2_U,FUNC_GPIO2);
	//GPIO_OUTPUT_SET(2,0);
	//gpio_output_set(BIT2,0,BIT2,0);//输出高
	gpio_output_set(0,BIT2,BIT2,0);//输出低
	gpio_output_set(BIT16,0,0,BIT16);//输入

}

void init_done_cb(void)
{
	char buf[64] = {0};
	to_scan();
	os_sprintf(buf, "compile time:%s %s", __DATE__, __TIME__);

	os_printf("uart init ok, %s\n", buf);
}
void user_rf_pre_init(void)
{
}
void user_init(void)
{
	struct station_config sta_conf;

	uart_init(BIT_RATE_9600, BIT_RATE_9600);
	GPIO_Init();
	wifi_set_opmode(0x03);//设置为AP模式
	os_memcpy(sta_conf.ssid,"TM5",strlen("test"));
	os_memcpy(sta_conf.password,"wubing123",strlen("wubing123"));
	wifi_station_set_config(&sta_conf);
	wifi_station_disconnect();
	wifi_station_connect();
	system_init_done_cb(init_done_cb);


	//os_timer_setfn(&tm,GetMyDuty,NULL);//回调函数
	//os_timer_arm(&tm,500,TRUE);//设置定时,重复
	pwm_init(1000,pwm_duty,1,io_info);
}

